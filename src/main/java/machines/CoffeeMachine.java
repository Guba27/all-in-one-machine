package machines;

import components.CoffeeCapsule;
import components.Cup;
import components.Sugar;

public class CoffeeMachine {
    private CoffeeCapsule coffeeCapsule;
    private Cup cup;
    private Sugar sugar;
    private int waterLevel;

    public CoffeeMachine(int initialWaterLevel, CoffeeCapsule coffeeCapsule, Cup cup, Sugar sugar) {
        this.waterLevel = initialWaterLevel;
        this.coffeeCapsule = coffeeCapsule;
        this.cup = cup;
        this.sugar = sugar;
    }

    public String brewCoffee() {
        if (coffeeCapsule != null && waterLevel > 0) {
            waterLevel--;
            return "Brewing coffee: " + coffeeCapsule.getType() + " coffee capsule with flavor "
                    + coffeeCapsule.getFlavor() + " and with intensity " + coffeeCapsule.getIntensity() +
                    " in a " + cup.getSize() + " cup with " + sugar.getType() + " sugar.";
        } else {
            return "No coffee capsule inserted";
        }
    }
}