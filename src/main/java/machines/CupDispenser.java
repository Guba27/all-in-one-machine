package machines;
import components.Cup;
public class CupDispenser {
    private Cup cup;
    private int cupLevel;

    public CupDispenser(Cup cup, int initialCupLevel) {
        this.cup = cup;
        this.cupLevel = initialCupLevel;
    }

    public String dispenseCup() {
        if (cup != null && cupLevel > 0) {
            cupLevel--;
            return "Dispensing " + cup.getSize() + " cup";
        } else {
            return "No cups available";
        }
    }
}