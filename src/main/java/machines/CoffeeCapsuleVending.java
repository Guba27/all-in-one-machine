package machines;

import components.CoffeeCapsule;

public class CoffeeCapsuleVending {
    private CoffeeCapsule coffeeCapsule;
    private int capsuleLevel;

    public CoffeeCapsuleVending(CoffeeCapsule coffeeCapsule, int initialCapsuleLevel) {
        this.coffeeCapsule = coffeeCapsule;
        this.capsuleLevel = initialCapsuleLevel;
    }

    public String dispenseCapsule() {
        if (coffeeCapsule != null && capsuleLevel > 0) {
            capsuleLevel--;
            return "Dispensing " + coffeeCapsule.getType() + " coffee capsule with flavor "
                    + coffeeCapsule.getFlavor() + " and with intensity " + coffeeCapsule.getIntensity();
        } else {
            return "No coffee capsules available";
        }
    }
}