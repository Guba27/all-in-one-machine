package machines;
public class AllInOneMachine {
    private CoffeeCapsuleVending capsuleDispenser;
    private CupDispenser cupDispenser;
    private SugarDispenser sugarDispenser;
    private CoffeeMachine coffeeMachine;

    public AllInOneMachine(CoffeeCapsuleVending capsuleDispenser, CupDispenser cupDispenser, SugarDispenser sugarDispenser, CoffeeMachine coffeeMachine) {
        this.capsuleDispenser = capsuleDispenser;
        this.cupDispenser = cupDispenser;
        this.sugarDispenser = sugarDispenser;
        this.coffeeMachine = coffeeMachine;
    }

    public String dispenseCoffee() {
        return capsuleDispenser.dispenseCapsule() + "\n" +
        sugarDispenser.dispenseSugar() + "\n" +
        cupDispenser.dispenseCup() + "\n" +
        coffeeMachine.brewCoffee();
    }
}