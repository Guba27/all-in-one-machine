package machines;

import components.Sugar;

public class SugarDispenser {
    private Sugar sugar;
    private int sugarLevel;

    public SugarDispenser(Sugar sugar, int initialSugarLevel) {
        this.sugar = sugar;
        this.sugarLevel = initialSugarLevel;
    }

    public String dispenseSugar() {
        if (sugar != null && sugarLevel > 0) {
            sugarLevel--;
            return "Dispensing " + sugar.getType() + " sugar";
        } else {
            return "No sugar available";
        }
    }
}