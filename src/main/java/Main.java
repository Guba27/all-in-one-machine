import components.CoffeeCapsule;
import components.Cup;
import components.Sugar;
import machines.*;

public class Main {
    public static void main(String[] args) {
        CoffeeCapsule flavor1Intensity5 = new CoffeeCapsule("Europian", "Cocoa", 5);
        Cup smallCup = new Cup("Small");
        Sugar brownSugar = new Sugar("Brown");
        CoffeeCapsuleVending coffeeCapsuleVending = new CoffeeCapsuleVending(flavor1Intensity5, 10);
        CupDispenser cupDispenser = new CupDispenser(smallCup, 20);
        SugarDispenser sugarDispenser = new SugarDispenser(brownSugar, 10);
        CoffeeMachine coffeeMachine = new CoffeeMachine(3, flavor1Intensity5, smallCup, brownSugar);
        AllInOneMachine allInOneMachine = new AllInOneMachine(coffeeCapsuleVending, cupDispenser, sugarDispenser, coffeeMachine);

        String result = allInOneMachine.dispenseCoffee();
        System.out.println(result);
    }
}