package components;

public class CoffeeCapsule {
    private String type;
    private String flavor;
    private int intensity;

    public CoffeeCapsule(String type, String flavor, int intensity) {
        this.type = type;
        this.flavor = flavor;
        this.intensity = intensity;
    }

    public String getType() {
        return type;
    }

    public String getFlavor() {
        return flavor;
    }

    public int getIntensity() {
        return intensity;
    }
}
