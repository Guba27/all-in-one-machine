package components;

public class Sugar {
    private String type;

    public Sugar(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
