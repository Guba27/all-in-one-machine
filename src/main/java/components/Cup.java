package components;

public class Cup {
    private String size;

    public Cup(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }
}